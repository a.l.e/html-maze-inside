# html-maze-inside

This maze runs under 04/ in the [main maze](https://gitlab.com/a.l.e/html-maze).

It has `04/index.html` and `04/05.html` then goes back to the main maze, in the `world/` directory.

